package com.dcm;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class ClientConnection implements Runnable {
    private String phoneNr;
    private String ipAddress;
    private Integer distPort;
    private Boolean isConnected;
    private List<Message> sendQue = new ArrayList<>();
    private List<Message> receiveQue = new ArrayList<>();


    public ClientConnection(String phoneNr, String ipAddress, Integer distPort) {
        this.phoneNr = phoneNr;
        this.ipAddress = ipAddress;
        this.distPort = distPort;
        this.isConnected = false;

    }


    public void connectionServerHandling(Socket destSocket) {
        Log.d("DCM_", "Successfully connected to server " + destSocket.getInetAddress());
        try (ObjectOutputStream os = new ObjectOutputStream(destSocket.getOutputStream());
             ObjectInputStream is = new ObjectInputStream(destSocket.getInputStream())) {

            new Thread(() -> {
                while (destSocket.isConnected()) {
                    try {
                        Message m = (Message) is.readObject();
                        Log.d("DCM_", "read message: " + m);
                        if (m != null && m.isValid()) {
                            Log.d("DCM_", "Valid object, adding to receiveQue");
                            receiveQue.add(m);
                        }
                        Thread.sleep(100);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            while (destSocket.isConnected()) {
                try {
                    if (!sendQue.isEmpty()) {
                        Log.d("DCM_", "Sending message: " + sendQue.get(sendQue.size()-1));
                        os.writeObject(sendQue.remove(sendQue.size() - 1));
                    } else Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public synchronized Boolean sendMessage(Message m) {
        if(m!=null) {
            sendQue.add(m);
            return true;
        } else return false;
    }

    public synchronized Message getMessage() {
        if(!receiveQue.isEmpty()) {
            return receiveQue.remove(receiveQue.size()-1);
        } else return null;
    }

    @Override
    public void run() {
        Log.d("DCM_","Trying to connect to: " + this.ipAddress + " : " + this.distPort);
        InetAddress serverAddr = null;
        try {
            serverAddr = InetAddress.getByName(this.ipAddress);
        } catch (UnknownHostException e) {
            Log.d("DCM_","Cant parses ip-address " + this.ipAddress);
            e.printStackTrace();
        }

        try(Socket clientSocket = new Socket(serverAddr,this.distPort)) {
            Log.d("DCM_","Successfully logged into server " + this.ipAddress + " : " + this.distPort);
            this.isConnected = true;
            connectionServerHandling(clientSocket);

        } catch (IOException e) {
            Log.d("DCM_","Failed to log into server " + this.ipAddress + " : " + this.distPort);
            e.printStackTrace();
        }

        Log.d("DCM_","Connection " + phoneNr + " ended.");
        this.isConnected = false;

    }


    public boolean isConnected() {
        return this.isConnected;
    }


}
