package com.dcm;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Message class of a friend. Stores only messages to friend, not from.
 */
public class Friend {
    private String userName;
    private String ipAddress;
    private Integer destPortNr;
    private String phoneNr;
    private List<Message> messagesFromFriend;
    private List<Message> myMessages;
    private ClientConnection connection;
    private MessageAdapter messageAdapter = null;
    private Context context;

    Friend(String phoneNr) {
        this.phoneNr = phoneNr;
        this.destPortNr = 0;
        this.ipAddress = "";
        if(!loadFromFile()) messagesFromFriend = new ArrayList<>();
        myMessages = new ArrayList<>();
    }

    Friend(String userName, String phoneNr, String ipAddress, Integer destPortNr, Context context) {
        this.context = context;
        this.userName = userName;
        this.ipAddress = ipAddress;
        this.destPortNr = destPortNr;
        this.phoneNr = phoneNr;
        if(!loadFromFile()) messagesFromFriend = new ArrayList<>();
        myMessages = new ArrayList<>();
    }

    public void Connect() {
        Log.d("DCM_", "Connecting...");
        new Thread(connection = new ClientConnection(this.phoneNr, this.ipAddress, this.destPortNr)).start();
/*
        new Thread(() -> {
            while (true) {
                retrieveMessages();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
*/
    }

    public Boolean saveToFile() {
        try(ObjectOutputStream os = new ObjectOutputStream(Files.newOutputStream(Paths.get(phoneNr + ".dat")))) {
            os.writeObject(this.messagesFromFriend);
            return true;
        } catch (IOException e) {
            System.err.println("Cant save file " + this.phoneNr + ".dat to the disk.");
            return false;
        }
    }
    public Boolean loadFromFile() {
        try(ObjectInputStream is = new ObjectInputStream(Files.newInputStream(Paths.get(phoneNr + ".dat")))) {
            this.messagesFromFriend = (List<Message>) is.readObject();
            return true;
        } catch (IOException e) {
            System.err.println("Cant find file " + this.phoneNr + ".dat on the disk. Have it been created?");
            return false;
        } catch (ClassNotFoundException e) {
            System.err.println("Cant read object type Set<Message> from file " + this.phoneNr + ".dat");
            return false;
        }

    }

    public boolean isConnected() {
        return connection.isConnected();
    }

    public void sendMessage(Message m) {
        myMessages.add(m);
        if(messageAdapter!=null) messageAdapter.insert(m, messageAdapter.getCount());
        connection.sendMessage(m);
    }

    public void retrieveMessages() {
        Message m = connection.getMessage();
        if(m!=null) {
            Log.d("DCM_","Retriving message from que: " + m.getMessage());
            this.messagesFromFriend.add(m);
            if(messageAdapter!=null) messageAdapter.insert(m, messageAdapter.getCount());
        }
    }

    public List<Message> getAllMessages() {
        List<Message> allMessages = new ArrayList<>();
        allMessages.addAll(this.myMessages);
        allMessages.addAll(this.messagesFromFriend);
        return allMessages;
    }


    public String getUserName() {
        return this.userName;
    }

    public void setMessageAdapter(MessageAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    public MessageAdapter getMessageAdapter() {
        return messageAdapter;
    }
}
