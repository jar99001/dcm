package com.dcm;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class FriendAdapter extends ArrayAdapter<Friend> {
    private Context context;
    private List<Friend> userList;


    public FriendAdapter(Context context, List<Friend> list) {
        super(context, 0, list);
        this.context = context;
        this.userList = list;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null) v = LayoutInflater.from(this.context).inflate(R.layout.user_item,parent,false);


        Friend currentFriend = userList.get(position);
        TextView userName = v.findViewById(R.id.userName);

        userName.setText(currentFriend.getUserName());

        userName.setOnClickListener(v1 -> {
            MessagingFragment messagingFragment = new MessagingFragment();

            Bundle args = new Bundle();
                args.putString("userName", currentFriend.getUserName());
//                args.putString("senderName", ((MainActivity)parent.getContext()).userModel.getUser().getUserName());
            messagingFragment.setArguments(args);


            ((AppCompatActivity)context)
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,messagingFragment,"MESSAGING")
                    .commit();

        });

        return v;
    }
}
