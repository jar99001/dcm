package com.dcm;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

public class HomeFragment extends Fragment {
    private UserViewModel userModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home, container, false);
        userModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);


        new ServerConnection(1234).execute();

        ImageView exitImageView = view.findViewById(R.id.backBtn);
        ListView userListView = view.findViewById(R.id.userListView);



        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Log.d("DCM_","ERROR");
            e.printStackTrace();
        }

        userModel.user = new User("0761702252","jar99001","Johan Allander","fam.allander@gmail.com");
        userModel.user.addFriend(new Friend("Ala Rebwar","07600000","localhost",1234, getContext()));
        userModel.user.ConnectToAll();

        userModel.user.getFriends().get(0).sendMessage(new Message("1", userModel.user.getUserName()));
        Message m = new Message("2", userModel.user.getUserName());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        userModel.user.getFriends().get(0).sendMessage(m);



        FriendAdapter userAdapter = new FriendAdapter(getContext(), userModel.user.getFriends());
        userListView.setAdapter(userAdapter);

        Message m2 = new Message("Hej till Ala", userModel.user.getUserName());
        userModel.user.getFriends().get(0).sendMessage(m2);


        exitImageView.setOnClickListener(v -> {
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        });


        /*
        Message m,m2 = null;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Log.d("DCM_","ERROR");
            e.printStackTrace();
        }

        while(u.friends.get(0).isConnected()) {
            m = new Message("Hej hopp");
            u.friends.get(0).sendMessage(m);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            u.friends.get(0).retrieveMessages();
            Set<Message> lm = u.friends.get(0).getMessages();
            if(lm!=null) Log.i("DCM_","Message: " + lm);
                else Log.d("DCM_","No message.");
        }
        */

        return view;
    }
}