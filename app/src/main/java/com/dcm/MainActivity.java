package com.dcm;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    UserViewModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userModel = ViewModelProviders.of(this).get(UserViewModel.class);

        Thread update = new Thread(() -> {
            while(true) {
                runOnUiThread(() -> {
                    if(userModel.user!=null) userModel.user.retrieveAllMessages();
                });

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        update.start();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentContainer, new HomeFragment())
                .commit();

    }
}
