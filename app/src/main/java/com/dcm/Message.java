package com.dcm;

import android.util.Log;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Message implements Serializable,Comparable<Message>  {
    private String message;
    private String senderName;
    private LocalDateTime created;

    private List<Attachment> attachments = new ArrayList<>();

    public Message(String message, String senderName) {
        this.message = message;
        this.senderName = senderName;
        this.created = LocalDateTime.now();
    }

    public boolean isValid() {
        if(message!=null && created.isBefore(LocalDateTime.now())) return true;
            else {
                Log.d("DCM_","Error, validity test failed.");
                return false;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", created=" + created +
                '}';
    }

    @Override
    public int compareTo(Message o) {
        return (this.created.getNano() - o.created.getNano());
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }


}
