package com.dcm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class MessageAdapter extends     ArrayAdapter<Message> {
    private Context context;
    private List<Message> messageList;

    public MessageAdapter(Context context, List<Message> list) {
        super(context,0 ,list);
        this.context = context;
        this.messageList = list;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) v = LayoutInflater.from(this.context).inflate(R.layout.message_item, parent, false);


        TextView userName = v.findViewById(R.id.userNameMessage);
        TextView message = v.findViewById(R.id.messageMessage);

        userName.setText(messageList.get(position).getSenderName() + ": ");
        message.setText(messageList.get(position).getMessage());

        return v;
    }
}
