package com.dcm;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class MessagingFragment extends Fragment {
    private String senderName;
    private UserViewModel userModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.messaging, container, false);

        userModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);

        TextView userName = v.findViewById(R.id.messaging_userName);
        EditText message = v.findViewById(R.id.messageTextEdit);
        Button sendMessageBtn = v.findViewById(R.id.sendMessageBtn);

        userName.setText(getArguments().getString("userName"));
        this.senderName = getArguments().getString("senderName");


//        User u = model.getSelected().getValue();

//        u.getFriends().get(0).getAllMessages();

        List<Message> allMessages = userModel.user.getFriends().get(0).getAllMessages();
        Collections.sort(allMessages);
        ListView messageListView = v.findViewById(R.id.messageListView);

        MessageAdapter messageAdapter = new MessageAdapter(getContext(),allMessages);
        messageListView.setAdapter(messageAdapter);
        userModel.user.getFriends().get(0).setMessageAdapter(messageAdapter);


        sendMessageBtn.setOnClickListener(view -> {
            Message m = new Message(message.getText().toString(), this.senderName);
            userModel.user.getFriends().get(0).sendMessage(m);
            message.setText("");

        });

        return v;
    }
}

