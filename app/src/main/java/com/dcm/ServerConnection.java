package com.dcm;

import android.os.AsyncTask;
import android.util.Log;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServerConnection extends AsyncTask {
    private Integer portNr;
    private ServerSocket serverSocket;
    private boolean serverOnline;
    List<Thread> clientsThread = new ArrayList<>();

    ServerConnection(Integer portNr) {
        this.portNr = portNr;
        this.serverOnline = true;
    }

    public void connectionClientHandling(Socket clientSocket) {
        Log.d("DCM_", "Successfully received connection from client " + clientSocket.getInetAddress());
        try (ObjectOutputStream os = new ObjectOutputStream(clientSocket.getOutputStream());
             ObjectInputStream is = new ObjectInputStream(clientSocket.getInputStream())
        ) {
            while(clientSocket.isConnected()) {
                Message m = (Message) is.readObject();
                Log.d("DCM_","Server: Resending message: " + m.getMessage());
                m.setSenderName("Ala Rebwar");
                os.writeObject(m);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        this.serverSocket = setupServerSocket(portNr);
        if(this.serverSocket!=null) {
            while(serverOnline) {
                try(Socket clientSocket = this.serverSocket.accept()) {
                    Log.d("DCM_", "Connection accepted from client socket.");
                    Thread thread = new Thread(() -> {
                        connectionClientHandling(clientSocket);
                    }, clientSocket.getInetAddress().toString());
                    clientsThread.add(thread);
                    thread.run();


                } catch (IOException e) {
                    Log.d("DCM_", "Failed to accept client socket connection.");
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Log.d("DCM_","Failed to create server socket.");
            return null;
        }

        return null;
    }

    private ServerSocket setupServerSocket(Integer portNr) {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(portNr);
            Log.d("DCM_","Server established, port: " + serverSocket.getLocalPort());
            return serverSocket;
        } catch (IOException e) {
            Log.d("DCM_","Cant start server on socket " + portNr);
            e.printStackTrace();
            return null;
        }
    }


}
