package com.dcm;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class User {
    private String phoneNr; // The only unique id
    private String userName;
    private String name;
    private String mail;
    private LocalDateTime created;
    private ServerConnection serverConnection;
    private List<Friend> friends = new ArrayList<>();

    public User(String phoneNr) {
        this.phoneNr = phoneNr;
        this.userName = "none";
        this.name = "none";
        this.mail = "none@none.com";
        this.created = LocalDateTime.now();
        serverConnection = new ServerConnection(1234);
    }


    public User(String phoneNr, String userName, String name, String mail) {
        this.phoneNr = phoneNr;
        this.userName = userName;
        this.name = name;
        this.mail = mail;
        serverConnection = new ServerConnection(1234);
    }



    public void saveAllMessages() {
        friends.forEach(friend -> {
            friend.saveToFile();
        });
    }

    public void loadAllMessages() {
        friends.forEach(friend -> {
            friend.loadFromFile();
        });
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }

    public void addFriend(Friend friend) {
        friends.add(friend);
    }

    public void ConnectToAll() {
        friends.forEach(friend -> {
            friend.Connect();
        });
    }

    public void retrieveAllMessages() {
        friends.forEach(friend -> {
            friend.retrieveMessages();
            if(friend.getMessageAdapter()!=null) friend.getMessageAdapter().notifyDataSetChanged();
        });
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }
}
