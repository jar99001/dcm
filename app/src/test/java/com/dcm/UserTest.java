package com.dcm;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("When a user is created.")
class UserTest {

    @Test
    @DisplayName("Test constructor.")
    public void test() {
        User u = new User("0761702252");
        Assertions.assertEquals(u.getPhoneNr(),"0761702252");

    }
}